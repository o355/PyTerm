Hi!
This is where PyTerm stores personalization files, note files, and all that fun stuff.
If you don't want to break PyTerm, don't modify or delete this folder, or mess it up.
This folder is completely harmless.
