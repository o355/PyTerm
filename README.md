# End of Development Notice:
PyTerm is no longer in development. I have shifted my efforts to PyWeather, and I generally don't want to work on PyTerm any more. Everything will stay as-is, including betas, indevs, etc. I'm happy to accept pull requests that fix issues, add features, etc.

Thank you for your understanding! PyTerm was a huge milestone in my Python career, and the first big program I made. PyWeather is now the successor to what PyTerm was.

# PyTerm (Python Terminal)
At the basic level, PyTerm is a "terminal emulator" for Python. But not really. It includes some...other stuff.

# Current Versions
* Stable - 1.14 (updated a LONG time ago)
* LTS - 1.15 (updated some time ago)
* Beta - 2.0 Beta 3 (released mid-Jan 2017)
* Indev - 2.0 Beta 3/Beta 4 (updated recently)

Use the Stable/LTS branch for the best experience. If you want to take a look at new features, albeit at the risk of bad stability, use the Beta/Indev branch.
